<?php
include("config/api.inc");
?>
<!DOCTYPE html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<script>
var APIServer = '<?php echo $APIhost; ?>';
var APIPath = '<?php echo $APIpath; ?>';

function onvote(vid) {
    $.ajax
	({ 
	    url: APIServer + APIPath + 'api/addVote.php',
	    data: {"voteID": vid},
	    type: 'post',
	    success: function(result)
	    {
	        $("#success_alert").show();
	    }
	});
}

function close_success() {
    $("#success_alert").hide();
}
</script>
</head>
<body>
<center>
<table>
    <tr>
        <td>
            <p><font size="160%">1. 薯⽚</font></p>
        </td>
        <td>&nbsp;</td>
        <td><button class="btn btn-outline-primary btn-lg" onclick="onvote(1)">Support</button>
        </td>
    </tr>
    <tr>
        <td>
            <p><font size="160%">2. 林林</font></p>
        </td>
        <td>&nbsp;</td>
        <td>
            <button class="btn btn-outline-primary btn-lg" onclick="onvote(2)">Support</button>
        </td>
    </tr>
    <tr>
        <td>
            <p><font size="160%">3. 正氣</font></p>
        </td>
        <td>&nbsp;</td>
        <td>
            <button class="btn btn-outline-primary btn-lg" onclick="onvote(3)">Support</button>
        </td>
    </tr>
</table>
<div id="success_alert" class="alert alert-success alert-dismissable fade show" style="display: none;">
    <button type="button" class="close" onclick="close_success();">&times;</button>
    <strong>Success!</strong> Your vote has been submitted.
</div>
</center>
</body>
</html>