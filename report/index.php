<?php
include("../config/db.inc");
include("../config/api.inc");
?>
<!DOCTYPE html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
<script>
var APIServer = '<?php echo $APIhost; ?>';
var APIPath = '<?php echo $APIpath; ?>';

$(document).ready(function() {
    var k1m = 0;
	var k2m = 0;
	var k3m = 0;
	$.ajax
	({ 
	    url: APIServer + APIPath + 'api/getVotes.php',
	    type: 'get',
	    success: function(result2)
	    {
            var result = $.parseJSON(result2);
            $.each(result, function(k, v) {
                if (k=='v1') {
                    $("#k1_val").text(v);
                } else if (k=='v2') {
                    $("#k2_val" ).text(v);
                } else if (k=='v3') {
                    $("#k3_val" ).text(v);
                } else if (k=='v1m') {
                    k1m = v;
                } else if (k=='v2m') {
                    k2m = v;
                } else if (k=='v3m') {
                    k3m = v;
                }
            });
            if (k1m==0 && k2m==0 && k3m==0) {
                $("#info_alert").show();
            } else {
                var data = [{
                    values: [k1m, k2m, k3m],
                    labels: ['薯⽚', '林林', '正氣'],
                    type: 'pie'
                }];

                var layout = {
                    height: 400,
                    width: 500
                };

                Plotly.newPlot('myDiv', data, layout);
            }
	    }
	});
});

function close_info() {
    $("#info_alert").hide();
}
</script>
</head>
<body>
<center>
<table>
    <tr>
        <td>
            <p><font size="160%">1. 薯⽚ : </font></p>
        </td>
        <td>&nbsp;</td>
        <td><font size="160%"><div id="k1_val">0</div></font>
        </td>
    </tr>
    <tr>
        <td>
            <p><font size="160%">2. 林林 : </font></p>
        </td>
        <td>&nbsp;</td>
        <td>
            <font size="160%"><div id="k2_val">0</div></font>
        </td>
    </tr>
    <tr>
        <td>
            <p><font size="160%">3. 正氣 : </font></p>
        </td>
        <td>&nbsp;</td>
        <td>
            <font size="160%"><div id="k3_val">0</div></font>
        </td>
    </tr>
</table>
<div id="info_alert" class="alert alert-success alert-dismissable fade show" style="display: none;">
    <button type="button" class="close" onclick="close_info();">&times;</button>
    <strong>Info!</strong> 0 votes during the last 10 minutes.
</div>
<p><h1>Last 10 Minutes Votes</h1></p>
<div id="myDiv"></div>
<script></script>
</center>
</body>
</html>